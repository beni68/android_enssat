package com.enssat.projetandroid.socket.client

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.enssat.projetandroid.socket.utils.*
import java.io.IOException
import java.net.InetAddress
import java.net.Socket
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class ClientSocket(val context: Context, val messageListener: (String) -> Unit, val connectionListener: (Boolean) -> Unit) {
    private val TAG = this.javaClass.simpleName

    private lateinit var socket: Socket

    private val mainThread = object : Executor {
        val handler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            handler.post(command)
        }
    }

    fun stop() {
        socket.close()
    }

    fun connect(serverIp:String, serverPort:Int?) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val address = InetAddress.getByName(serverIp)
                val port = serverPort ?: 6791
                Log.d(TAG, "try connecting to $address:$port")
                socket = Socket(address, port)
                mainThread.execute { connectionListener(true) }


                // Recevoir nom salle
                val buffer = ByteArray(2048)
                val len = socket.getInputStream().read(buffer)
                val str = String(buffer, 0, len, StandardCharsets.UTF_8)

                if (Message.fromJson(str).type == "roomName") {
                    val message: RoomNameMessage = Message.fromJsonRoomName(str)
                    // Affiche le message reçu dans l'ui
                    mainThread.execute { messageListener("Estimer : \"" + message.name + "\"") }
                }

                listen()
            }
            catch(ex:Exception) {
                val msg = ex.message ?: "unable to connect server"
                mainThread.execute { messageListener(msg) }
                mainThread.execute { connectionListener(false) }
            }
        }
    }

    fun listen() {
        var loop = true

        // Écouter le serveur
        while (loop) {
            val buffer = ByteArray(2048)
            val len = socket.getInputStream().read(buffer)
            val msg = String(buffer, 0, len, StandardCharsets.UTF_8)
            Log.d("ResVoteMsg", msg)

            if (Message.fromJson(msg).type == "stopVote") {
                loop = false

                val messageRes: StopVoteMessage = Message.fromJsonEndVote(msg)
                Log.d("ResVoteRes", messageRes.res)
                mainThread.execute { messageListener("Votes terminés, la majorité a estimé la difficulté à " + messageRes.res) }

                // Lance une nouvelle activité avec le résultat des votes
                mainThread.execute {
                    stop()
                }
            } else if (Message.fromJson(msg).type == "ok") {
                val messageOk: OkMessage = Message.fromJsonOk(msg)
                mainThread.execute { messageListener(messageOk.msg) }
            }
        }
    }

//    fun sendAndReceive(msg: Message) {
//        Executors.newSingleThreadExecutor().execute {
//            try {
//                val data = Message.toJson(msg).toByteArray(StandardCharsets.UTF_8)
//                socket.getOutputStream().write(data)
//                socket.getOutputStream().flush()
//
//                val buffer = ByteArray(2048)
//                val len = socket.getInputStream().read(buffer)
//                val str = String(buffer, 0, len, StandardCharsets.UTF_8)
//
//                //affiche le message reçu dans l'ui
//                mainThread.execute { messageListener(str) }
//
//            } catch (e: IOException) {
//                val msg = e.message ?: "unable to send to or receive from server"
//                mainThread.execute { messageListener(msg) }
//                mainThread.execute { connectionListener(false) }
//                stop(msg)
//            }
//        }
//    }

    fun sendVote(msg: VoteMessage) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val data = Message.toJson(msg).toByteArray(StandardCharsets.UTF_8)
                socket.getOutputStream().write(data)
                socket.getOutputStream().flush()

            } catch (e: IOException) {
                mainThread.execute { connectionListener(false) }
                stop()
            }
        }
    }
}
