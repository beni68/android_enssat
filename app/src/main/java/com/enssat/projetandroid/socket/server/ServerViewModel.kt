package com.enssat.projetandroid.socket.server

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ServerViewModel(context: Context, roomName: String): ViewModel() {

    private val _msgSet = mutableSetOf<String>()
    private val _server = ServerSocket(context, roomName, this::onReceiveMessage)
    private val _allMessages = MutableLiveData<List<String>>()
    private val votes = arrayListOf<Double>()
    private val cardValues = listOf(0.0, 0.5, 1.0, 2.0, 3.0, 5.0, 8.0, 13.0, 20.0, 40.0, 100.0)

    val messages: LiveData<List<String>> get() = _allMessages

    init {
        _server.startListening()
    }

    override fun onCleared() {
        _server.stopListening()
        super.onCleared()
    }

    fun onReceiveMessage(msg: String, vote: String) {
        _msgSet.add(msg)
        _allMessages.postValue(_msgSet.toList())
        votes.add(vote.toDouble())
    }

    fun endVotes() {
        _server.endVotes(moyenneVotes(votes))
    }

    fun moyenneVotes(liste: ArrayList<Double>): Double {
        var total = 0.0

        liste.forEach {
            total += it
        }

        val moyenne = total / liste.size

        // On cherche la valeur de carte la plus proche de la moyenne
        for (i in 0 until cardValues.size-1) {
            if (cardValues[i] <= moyenne && cardValues[i+1] >= moyenne) {
                return if (moyenne - cardValues[i] > cardValues[i+1] - moyenne) {
                    cardValues[i+1]
                } else {
                    cardValues[i]
                }
            }
        }

        // Normalement n'arrive pas car la moyenne ne peut pas être plus grande que la plus grande carte
        return cardValues[cardValues.size-1]
    }
}