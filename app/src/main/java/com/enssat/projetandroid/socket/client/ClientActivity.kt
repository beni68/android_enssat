package com.enssat.projetandroid.socket.client

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.enssat.projetandroid.R
import com.enssat.projetandroid.databinding.ActivitySocketClientBinding
import com.enssat.projetandroid.socket.server.ServerSocket
import com.enssat.projetandroid.socket.utils.MessageAdapter
import com.enssat.projetandroid.socket.utils.VoteMessage


class ClientActivity : AppCompatActivity() {
    lateinit var binding: ActivitySocketClientBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_socket_client)

        val model = ViewModelProviders.of(this, ClientViewModelFactory(this)).get(ClientViewModel::class.java)

        val sharedPref = getSharedPreferences(getString(R.string.prefsName), Context.MODE_PRIVATE)
        val name = sharedPref.getString(getString(R.string.userName), "?")

        val host = intent.getStringExtra(getString(R.string.extra_ip_salle_to_conect))

        model.connect(host!!, ServerSocket.PORT)

        model.connected.observe(this, Observer { bool->
            binding.sendButton.isEnabled = bool
        })

        binding.sendButton.setOnClickListener { view ->
            model.sendVote(VoteMessage(
                name!!,
                binding.voteSpinner.selectedItem.toString(),
                binding.editTextVote.text.toString())
            )
            binding.sendButton.isEnabled = false
        }

        val adapter = MessageAdapter {}
        binding.messageList.adapter = adapter

        model.messages.observe(this, Observer { list ->
            adapter.list = list
        })
    }
}