package com.enssat.projetandroid.socket.server

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.enssat.projetandroid.R
import com.enssat.projetandroid.databinding.ActivitySocketServerBinding
import com.enssat.projetandroid.socket.utils.MessageAdapter
import com.enssat.projetandroid.socket.utils.NetworkUtils

class ServerActivity : AppCompatActivity() {
    lateinit var binding: ActivitySocketServerBinding
    lateinit var nomSalle: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // On met le nom de la salle dans l'actionbar
        nomSalle = intent.getStringExtra(getString(R.string.extra_nom_salle))!!
        supportActionBar?.title = nomSalle

        val model = ViewModelProviders.of(this, ServerViewModelFactory(this, nomSalle)).get(ServerViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_socket_server)

        // Affichage de l'ip
        binding.ipServer.text = NetworkUtils.getIpAddress(this).toString()

        val adapter = MessageAdapter {}
        binding.messageList.adapter = adapter

        model.messages.observe(this, Observer { list ->
            adapter.list = list
        })

        binding.endVotesBtn.setOnClickListener { view ->
            model.endVotes()
        }
    }
}
