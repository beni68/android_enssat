package com.enssat.projetandroid.socket.server

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.enssat.projetandroid.socket.utils.*
import java.io.IOException
import java.net.ServerSocket
import java.net.Socket
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class ServerSocket(val context: Context, val roomName: String, val listener: (String, String) -> Unit) {
    private val TAG = this.javaClass.simpleName

    companion object {
        val PORT = 6791
    }

    private lateinit var serverSocket: ServerSocket
    private val IPADDRESS = NetworkUtils.getIpAddress(context)
    private val MAX_CLIENT_BACK_LOG = 50
    private var loop = true

    //liste des clients connectés au serveur
    private val allClientsConnected = mutableListOf<Reply>()

    fun startListening() {
        serverSocket = ServerSocket(PORT, MAX_CLIENT_BACK_LOG, IPADDRESS)

        // écoute toutes les nouvelles demandes de connections clientes
        // et crée une socket locale au serveur, reply dédiée a ce nouveau client.
        Executors.newSingleThreadExecutor().execute {
            while (loop) {
                val newSocket = serverSocket.accept()
                allClientsConnected.add(Reply(newSocket, roomName, listener))
            }
        }
    }

    fun stopListening() {
        loop = false
        allClientsConnected.forEach { it.stop() }
        serverSocket.close()
    }

    fun endVotes(resultat: Double) {
        allClientsConnected.forEach { it.sendRes(resultat) }
        stopListening()
    }


    class Reply(val socket: Socket, val roomName: String, val listener: (String, String) -> Unit) {
        val TAG = this.javaClass.simpleName
        var loop = true
        val address = socket.inetAddress.address.toString()

        private val mainThread = object : Executor {
            val handler = Handler(Looper.getMainLooper())
            override fun execute(command: Runnable) {
                handler.post(command)
            }
        }

        init {
            Executors.newSingleThreadExecutor().execute {
                try {
                    sendRoomName()
                    while (loop) {
                        val buffer = ByteArray(2048)
                        val len = socket.getInputStream().read(buffer)
                        val msg = String(buffer, 0, len, StandardCharsets.UTF_8)
                        val messageVote: VoteMessage = Message.fromJsonVote(msg)

                        //affiche le message reçu dans l'ui
                        mainThread.execute { listener(messageVote.nom + " > a voté " + messageVote.vote + " \"" + messageVote.msg + "\"", messageVote.vote) }

                        //répond avec confirmation vote reçu
                        val data =
                            Message.toJson(OkMessage("Vote pris en compte"))
                                .toByteArray(StandardCharsets.UTF_8)
                        socket.getOutputStream().write(data)
                        socket.getOutputStream().flush()
                    }
                } catch (e: IOException) {
                    Log.d(TAG, "Client $address down")
                }
            }
        }

        fun sendRoomName () {
            val data =
                Message.toJson(RoomNameMessage(roomName))
                    .toByteArray(StandardCharsets.UTF_8)
            socket.getOutputStream().write(data)
            socket.getOutputStream().flush()
        }

        fun sendRes(res: Double) {
            Executors.newSingleThreadExecutor().execute {
                val data =
                    Message.toJson(StopVoteMessage(res.toString()))
                        .toByteArray(StandardCharsets.UTF_8)
                socket.getOutputStream().write(data)
                socket.getOutputStream().flush()
            }
        }

        fun stop() {
            loop = false
//            socket.close()
        }
    }
}