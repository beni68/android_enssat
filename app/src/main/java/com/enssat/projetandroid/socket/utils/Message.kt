package com.enssat.projetandroid.socket.utils

import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

sealed class Message constructor(val type: String) {
    companion object {
        private val gson = GsonBuilder().registerTypeAdapter(Message::class.java, MessageSerializer()).create()
        fun toJson(src: Message) = gson.toJson(src)
        fun fromJson(str: String) = gson.fromJson<Message>(str, Message::class.java)
        fun fromJsonVote(str: String) = gson.fromJson<VoteMessage>(str, VoteMessage::class.java)
        fun fromJsonEndVote(str: String) = gson.fromJson<StopVoteMessage>(str, StopVoteMessage::class.java)
        fun fromJsonRoomName(str: String) = gson.fromJson<RoomNameMessage>(str, RoomNameMessage::class.java)
        fun fromJsonOk(str: String) = gson.fromJson<OkMessage>(str, OkMessage::class.java)
    }
}

class UnknownMessage(): Message("unknown")

class StopVoteMessage(val res: String): Message("stopVote")

class OkMessage(val msg: String): Message("ok")

class RoomNameMessage(val name: String): Message("roomName")

class VoteMessage(val nom: String, val vote: String, val msg: String): Message("vote")

class MessageSerializer: JsonDeserializer<Message> {

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Message {
        val type = json.asJsonObject["type"].asString
        return when (type) {
            "stopVote" -> context.deserialize<StopVoteMessage>(json, StopVoteMessage::class.java)
            "ok" -> context.deserialize<OkMessage>(json, OkMessage::class.java)
            "roomName" -> context.deserialize<RoomNameMessage>(json, RoomNameMessage::class.java)
            "vote" -> context.deserialize<VoteMessage>(json, VoteMessage::class.java)
            else -> UnknownMessage()
        }
    }

}