package com.enssat.projetandroid

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.enssat.projetandroid.socket.client.ClientActivity
import com.enssat.projetandroid.socket.server.ServerActivity

class JoinOrCreateRoomActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_or_create_room)

        findViewById<Button>(R.id.creerSalle).setOnClickListener {
            val intent = Intent(this, ServerActivity::class.java)
            intent.putExtra(getString(R.string.extra_nom_salle), findViewById<EditText>(R.id.ipSalleCreer).text.toString())
            startActivity(intent)
        }

        findViewById<Button>(R.id.rejoindreSalleMulticast).setOnClickListener{
            val intent = Intent (this, ClientActivity::class.java)
            intent.putExtra(getString(R.string.extra_ip_salle_to_conect),findViewById<EditText>(R.id.ipSalleAJoindre).text.toString())
            startActivity(intent)
        }
    }
}