package com.enssat.projetandroid

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.multicastBtn).setOnClickListener { view ->
            val userName = findViewById<EditText>(R.id.userName).text.toString()

            if (userName != "") {
                // Saving user name
                val sharedPref = getSharedPreferences(getString(R.string.prefsName), Context.MODE_PRIVATE)
                with(sharedPref.edit()) {
                    putString(getString(R.string.userName), userName)
                    commit()
                }

                startActivity(Intent(this, JoinOrCreateRoomActivity::class.java))
            }
        }
    }
}
